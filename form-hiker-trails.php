<?php
include 'top.php'
?>

<?php
// this example creates a list box from our database.

//initialize value
$tblHikers = "Jiesheng Zhang";
$tblHikersERROR = false;

$Camel = "Camel's Hump";
$snake = "Snake Mountain";
$Rock = "Prospect Rock";
$Pond = "Skylight Pond";
$Mount = "Mount Pisgah";

$CamelError = false; 
$snakeError = false;
$RockError = false;
$PondError = false;
$MountError = false;

$dataRecord = array();
$hiker = "";
$trail = "";
$date = date("Y-m-d");


// Step Two: create your query
$query  = "SELECT `pmkHikersId`, `fldFirstName`, `fldLastName` FROM `tblHikers`";

// Step Three: run your query being sure to implement security
if ($thisDatabaseReader->querySecurityOk($query, 0)) {
    $query = $thisDatabaseReader->sanitizeQuery($query);
    $Jakey = $thisDatabaseReader->select($query, '');
}

// Step Four: prepare html output

print "<h2>Hiker</h2>";

if (isset($_POST["btnSubmit"])) {
    $thisURL = DOMAIN . PHP_SELF;
    if (!securityCheck($thisURL)) {
        $msg = '<p>This is not the right page to enter</p>';
        $msg.= '<p>Security check for it </p>';
        die($msg);
    }
$hikerERROR = false;
$dateERROR = false;
$trailERROR = false;

$errorMsg = array();
$data = array();
$dataEntered = false;

if (isset($_POST["tblHikers"])) {
    $hiker = htmlentities($_POST["tblHikers"], ENT_QUOTES, "UTF-8");
    $data[] = $hiker;
}


if (isset($_POST["TrailED"])) {
    $trail = htmlentities($_POST["TrailED"], ENT_QUOTES, "UTF-8");
    $data[] = $trail;
}


if (isset($_POST["shijian"])) {
    $date = htmlentities($_POST["shijian"], ENT_QUOTES, "UTF-8");
    $data[] = $date;
}

if ($hiker == "") {
    $errorMsg[] = "Please choose your tblHikers";
    $hikerERROR = true;
}


if ($trail == "") {
    $errorMsg[] = "Please pick the trail";
    $trailERROR = true;
}

if ($date == "") {
    $errorMsg[] = "Please enter your hiking date";
    $dateERROR = true;
}elseif(!verifyDate($date)){
        $errorMsg[] = "Please enter the right  hiking date format ";
        $dateERROR = true;
    }

foreach ($errorMsg as $Error) {
    echo "$Error <br>";
}

if (count($errorMsg) == 0) {

    $dataEntered = false;
    try {
        $thisDatabaseWriter->db->beginTransaction();

        $query = 'INSERT INTO tblHikersTrails SET ';

        $query .= 'fnkHikersId = ?,';
        $query .= 'fnkTrailsId = ?,';
        $query .= 'fldDateHiked = ?';

        if (DEBUG) {
            $thisDatabaseWriter->TestSecurityQuery($query, 0);
            print_r($data);
        }

        if ($thisDatabaseWriter->querySecurityOk($query, 0)) {
            $query = $thisDatabaseWriter->sanitizeQuery($query);
            $results = $thisDatabaseWriter->insert($query, $data);
            $primaryKey = $thisDatabaseWriter->lastInsert();

            if (DEBUG) {
                print "<p>pmk= " . $primaryKey;
            }
        }
        // all sql statements are done so lets commit to our changes

        $dataEntered = $thisDatabaseWriter->db->commit();
        if ($dataEntered = true){
            print "<h1>You are successful to register</h1> ";
        }else {
        
                if ($errorMsg) {    
                    print '<div id="errors">' . PHP_EOL;
                    print '<h2>Your form has the following mistakes that need to be fixed.</h2>' . PHP_EOL;
                    print '<ol>' . PHP_EOL;
                    foreach ($errorMsg as $err) {
                        print '<li>' . $err . '</li>' . PHP_EOL;       
                    }
                    print '</ol>' . PHP_EOL;
                    print '</div>' . PHP_EOL;
                }
            }
        if (DEBUG)
            print "<p>Your transaction was complete ";
    } catch (PDOExecption $e) {
        $thisDatabase->db->rollback();
        if (DEBUG)
            print "Error!: " . $e->getMessage() . "</br>";
        $errorMsg[] = "There was a problem with accepting and your data, So you should contact us.";
    }
}
}
print '<form action="' . PHP_SELF . '"';
print ' id = "frmRegister"';
print ' method = "post">';


print '<label for="tblHikers"';
if ($tblHikersERROR) {
    print ' class = "mistake"';
}

print '> ';
print '<select id="tblHikers" ';
print '        name="tblHikers"';
print '        tabindex="300" >';


foreach ($Jakey as $tblJakey) {

    print '<option ';
    if ($tblJakey["pmkHikersId"] == $hiker){
       print 'selected ';
    }
    print 'value="' . $tblJakey["pmkHikersId"] . '">' . $tblJakey["fldFirstName"] . 
             " " . $tblJakey["fldLastName"];

    print '</option>';
}

print '</select></label>';

print "<h2>Hiking date</h2>";
print '<input type="date" name="shijian" value="'. $date .'">';

// Step Two: create your query
$query = "SELECT `pmkTrailsId`, `fldTrailName`, `fldTotalDistance`, `fldHikingTime`, `fldVerticalRise`, `fldRating` FROM `tblTrails`";

// Step Three: run your query being sure to implement security
$Bob = "";

if ($thisDatabaseReader->querySecurityOk($query, 0)) {
    $query = $thisDatabaseReader->sanitizeQuery($query);
    $Bob = $thisDatabaseReader->select($query, '');
}

$ERROR = false;

// Step Four: prepare html output, I used an output array for this sample
$output = array();
$output[]= '<h2>Trail</h2>';
//$output[]= '<form>';
$output[]= '<fieldset class="radio ';
if ($ERROR) {
    $output[] = ' mistake';
}
$output[] = '">';


if (is_array($Bob)) {
    foreach ($Bob as $trail) {

        $output[]= '<input type="radio" ';
        $output[]= ' name="TrailED" ';

// -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-      
// there has to be a better way to make the check boxes sticky as we need to 
// hardcode this each time we get a new activity
        /*if (str_replace(" ", "-", $trail["fldTrailName"]) == "Camel's Hump") {
            if ($Camel) {
                $output[] = ' checked ';
            }
        }

        if (str_replace(" ", "-", $trail["fldTrailName"]) == 'Snake Mountain') {
            if ($snake) {
                $output[] = ' checked ';
            }
        }

        if (str_replace(" ", "-", $trail["fldTrailName"]) == 'Prospect Rock') {
            if ($Rock) {
                $output[] = ' checked ';
            }
        }

        if (str_replace(" ", "-", $trail["fldTrailName"]) == 'Skylight Pond') {
            if ($Pond) {
                $output[] = ' checked ';
            }
        }

        if (str_replace(" ", "-", $trail["fldTrailName"]) == 'Mount Pisgah') {
            if ($Mount) {
                $output[] = ' checked ';
            }
        }*/
        if ($trail["pmkTrailsId"] == $trail) {
            $output[] = 'checked ';
        }

// -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
        
        $output[] = 'value="' . $trail["pmkTrailsId"] . '">' . $trail["fldTrailName"];
        $output[] = '</label>';
    }

    $output[] = '</fieldset>';
    print join("\n", $output);  // this prints each line as a separate  line in html
            }

?>
<fieldset class="buttons">
    <legend></legend>
    <input class="button" id="btnSubmit" name="btnSubmit" tabindex="900" type="submit" value="Register" >
</fieldset>

<?php 
print '</form>';
include "footer.php"; 
?>
