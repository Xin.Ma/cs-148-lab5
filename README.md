Git global setup
git config --global user.name "Xin Max Ma"
git config --global user.email "xin.ma@uvm.edu"

Create a new repository
git clone git@gitlab.uvm.edu:Xin.Ma/cs-148-lab5.git
cd cs-148-lab5
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder
cd existing_folder
git init
git remote add origin git@gitlab.uvm.edu:Xin.Ma/cs-148-lab5.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.uvm.edu:Xin.Ma/cs-148-lab5.git
git push -u origin --all
git push -u origin --tags