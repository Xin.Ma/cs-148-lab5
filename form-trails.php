 
<?php
include 'top.php'
?>

<?php
//Initialize the query 
$query = "SELECT `fldTrailName`, `fldTotalDistance`, `fldHikingTime`, `fldVerticalRise`, `fldRating` FROM `tblTrails`";

$query = "SELECT `pmkTag` FROM `tblTags`";

//Initialization for the values
$update = false;
$TrailName = "";
$TotalDistance = "";
$HikingTime = "";
$VerticalRise = "";
$Rating = "";
$Tag = "";
$tags = "";
$ratingERROR = false;
$pmkTrailsId = -1;
$sticky = array();

// Create the query from the table
if (isset($_GET["update"])) {
    $pmkTrailsId = (int) htmlentities($_GET["update"], ENT_QUOTES, "UTF-8");
    
    $query = "SELECT fldTrailName, fldTotalDistance, fldHikingTime, fldVerticalRise, fldRating "; 
    $query .= "FROM tblTrails where pmkTrailsId = ? ";
    
    $data = array($pmkTrailsId);

    
    if ($thisDatabaseReader->querySecurityOk($query, 1)) {
        $query = $thisDatabaseReader->sanitizeQuery($query);
        $TRAIL = $thisDatabaseReader->select($query, $data);
    }

           
    $TrailName = $TRAIL[0]["fldTrailName"];
    $TotalDistance = $TRAIL[0]["fldTotalDistance"];
    $HikingTime = $TRAIL[0]["fldHikingTime"];
    $VerticalRise = $TRAIL[0]["fldVerticalRise"];
    $Rating = $TRAIL[0]["fldRating"];
    
    
}


//%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%
//
// SECTION: 1d form error flags
//
// Initialize Error Flags one for each form element we validate
// in the order they appear in section 1c.
$NameTrailERROR = false;
$TotalDistanceERROR = false;
$HikingTimeERROR = false;
$VerticalRiseERROR = false;
$RatingERROR = false;
$TagERROR = false;

// create array to hold error messages filled (if any) in 2d displayed in 3c.
$errorMsg = array();

$data2 = array();

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//
// SECTION: 2 Process for when the form is submitted
//

//prepare html output
if (isset($_POST["btnSubmit"])) {

    $pmkTrailsId = (int) htmlentities($_POST["hidTrailsId"], ENT_QUOTES, "UTF-8");
        if ($pmkTrailsId > 0) {
            $update = true;
        }
    // make the html names to display
    $TrailName = htmlentities($_POST["txtName"], ENT_QUOTES, "UTF-8");

    $TotalDistance = htmlentities($_POST["txtTotalDistance"], ENT_QUOTES, "UTF-8");

    $HikingTime = htmlentities($_POST["txtHikingTime"], ENT_QUOTES, "UTF-8");


    $VerticalRise = htmlentities($_POST["txtVerticalRise"], ENT_QUOTES, "UTF-8");

    $Rating = htmlentities($_POST["txtRating"], ENT_QUOTES, "UTF-8");
    
    $Tag = htmlentities($_POST["tag"], ENT_QUOTES, "UTF-8");
    $Tag = $_POST["tag"];
    foreach ($Tag as $tag) {
        $sticky[] = htmlentities($tag, ENT_QUOTES, "UTF-8");
    }


// Validate the input    
if ($TrailName == "") {
    $errorMsg[] = "Please enter the name";
    $NameTrailERROR = true;
}elseif (!verifyAlphaNum($TrailName)) {
        $errorMsg[] = "Your name appears to string.";
        $NameTrailERROR = true;
}elseif (verifyNumeric($TrailName)) {
        $errorMsg[] = "Your name appears to string.";
        $NameTrailERROR = true;
}

if ($TotalDistance == "") {
    $errorMsg[] = "Please enter the distance";
    $TotalDistanceERROR = true;
}elseif (!verifyNumeric($TotalDistance)) {
        $errorMsg[] = "Your TotalDistance appears to number.";
        $TotalDistanceERROR = true;
}

if ($HikingTime == "") {
    $errorMsg[] = "Please enter the time";
    $HikingTimeERROR = true;
}elseif (!verifyTime($HikingTime)) {
        $errorMsg[] = "Please use the HH:MM:SS format";
        $HikingTimeERROR = true;
}

if ($VerticalRise == "") {
    $errorMsg[] = "Please enter the rise";
    $VerticalRiseERROR = true;
}elseif (!verifyNumeric($VerticalRise)) {
        $errorMsg[] = "Your VerticalRise appears to number.";
        $VerticalRiseERROR = true;
}

if ($Rating == "") {
    $errorMsg[] = "Please choose the rating";
    $RatingERROR = true;
}

if ($Tag == "") {
    $errorMsg[] = "Please choose the Trail";
    $TagERROR = true;
}

foreach ($errorMsg as $Error) {
    echo "$Error <br>";
}

if (!$errorMsg) {

    $dataEntered = false;
    $data = array();
//    $data[] = $TrailName;
//    $data[] = $TotalDistance;
//    $data[] = $HikingTime;
//    $data[] = $VerticalRise;
//    $data[] = $Rating;
    $data2[] = $Tag;
    
    try {
        $thisDatabaseWriter->db->beginTransaction();
    if ($update) {
        $query = 'UPDATE tblTrails SET ';
    } else {
        $query = 'INSERT INTO tblTrails SET ';
            }
        $query .= 'fldTrailName = ?, ';
        $query .= 'fldTotalDistance = ?, ';
        $query .= 'fldHikingTime = ?, ';
        $query .= 'fldVerticalRise = ?, ';
        $query .= 'fldRating = ?';

        if (DEBUG) {
            $thisDatabaseWriter->TestSecurityQuery($query, 0);
            print_r($data);
        }
        if ($update) {
                $query .= 'WHERE pmkTrailsId = ?';
                $data[] = $pmkTrailsId;

                if ($thisDatabaseReader->querySecurityOk($query, 1)) {
                    $query = $thisDatabaseWriter->sanitizeQuery($query);
                    $results = $thisDatabaseWriter->update($query, $data);
                }
            } else {
            if ($thisDatabaseWriter->querySecurityOk($query, 0)) {
                $query = $thisDatabaseWriter->sanitizeQuery($query);
                $results = $thisDatabaseWriter->insert($query, $data);
                $primaryKey = $thisDatabaseWriter->lastInsert();
            }
            }
            if (DEBUG) {
                print "<p>pmk= " . $primaryKey;
            }
          
    
        // all sql statements are done so lets commit to our changes

        $dataEntered = $thisDatabaseWriter->db->commit();
        if ($dataEntered = true)
            print "You are success!";
        
        if (DEBUG)
            print "<p>transaction complete ";
    } catch (PDOExecption $e) {
        $thisDatabase->db->rollback();
        if (DEBUG)
            print "Error!: " . $e->getMessage() . "</br>";
        $errorMsg[] = "There was a problem with accepting your data please contact us directly.";
    }

try {
        $thisDatabaseWriter->db->beginTransaction();
        foreach ($sticky as $stick) {
                
              
            //$query = 'INSERT INTO tblTrailsTags SET ';
            //$query .= 'pfkTrailsId= ?, pfkTags = ?';
            
            //$data2[] = $pmkTrailsId;
            $data2[] = $stick;

            if ($update) {
                $query = 'UPDATE tblTrailsTags SET ';
            }else{
                $query = 'INSERT INTO tblTrailsTags SET ';
            }    
            $query .= 'pfkTags = ? ';
            if (DEBUG) {
                $thisDatabaseWriter->TestSecurityQuery($query, 0);
                print_r($data2);
            }
            if ($update){
                $query .= 'WHERE pfkTrailsId = ?';
                $data2[] = $pfkTrailsId;
            
                if ($thisDatabaseReader->querySecurityOk($query, 1)) {
                    $query = $thisDatabaseWriter->sanitizeQuery($query);
                    $results = $thisDatabaseWriter->update($query, $data2);
                }
            }else{
                if ($thisDatabaseWriter->querySecurityOk($query, 0)) {
                    $query = $thisDatabaseWriter->sanitizeQuery($query);
                    $results = $thisDatabaseWriter->insert($query, $data2);
                    $primaryKey = $thisDatabaseWriter->lastInsert();
                }
            }
            if (DEBUG) {
                print "<p>pmk= " . $primaryKey;
            }

        // all sql statements are done so lets commit to our changes
        }
        $dataEntered = $thisDatabaseWriter->db->commit();
        if ($dataEntered = true){
            print "!";
         
        }
        if (DEBUG)
            print "<p>transaction complete ";
    } catch (PDOExecption $e) {
        $thisDatabase->db->rollback();
        if (DEBUG)
            print "Error!: " . $e->getMessage() . "</br>";
        $errorMsg[] = "There was a problem with accepting your data please contact us directly.";
    }
  }
}

// Main content 
if($isAdmin){
print '<form action="' . PHP_SELF . '"';
print ' id = "frmRegister"';
print ' method = "post">';

print '<input type="hidden" name="hidTrailsId" value="' . $pmkTrailsId . '">';

print "<h2>Name</h2>";
print '<input type="text" name="txtName"  value="' . $TrailName . '">';

print "<h2>Total Distance</h2>";
print '<input type="number" name="txtTotalDistance" step="0.1" value="' . $TotalDistance . '" min="1" >';

print "<h2>Hiking Time</h2>";
print '<input type="time" name="txtHikingTime" value="' . $HikingTime . '" >';

print "<h2>Vertical Rise</h2>";
print '<input type="number" name="txtVerticalRise" value="' . $VerticalRise . '" min="1" >';

print "<h2>Rating</h2>";
print '<select name="txtRating" id="rating">';
print '<option value = ""> - Please select a rating - </option>';
print '<option value = "Easy"' . ($Rating=='Easy'? ' selected ': ''). '> Easy </option>';
print '<option value = "Moderate"' . ($Rating=='Moderate'? ' selected ': ''). '> Moderate </option>';
print '<option value = "Moderately Strenuous"' . ($Rating=='Moderately Strenuous'? ' selected ': ''). '> Moderately Strenuous </option>';
print '<option value = "Strenuous"' . ($Rating=='Strenuous'? ' selected ': ''). '> Strenuous </option>';
print '</select>';


$query = "SELECT `pmkTag` FROM `tblTags` ";



$tags = "";
$tagsERROR = false;

if ($thisDatabaseReader->querySecurityOk($query, 0)) {
    $query = $thisDatabaseReader->sanitizeQuery($query);
    $tags = $thisDatabaseReader->select($query, '');
}


$output = array();
$output[] = '<h2>Trail</h2>';
$output[] = '<fieldset class="checkbox ';
if ($tagsERROR) {
    $output[] = ' mistake';
}
$output[] = '">';

if (is_array($tags)) {
    $query = "SELECT `pfkTag` ";
    $query .= "FROM `tblTrailsTags` WHERE `pfkTrailsId` = ? ";
    $data3 = array($pmkTrailsId);
        
    $check = "";
    if ($thisDatabaseReader->querySecurityOk($query, 1)) {
        $query = $thisDatabaseReader->sanitizeQuery($query);
        $check = $thisDatabaseReader->select($query, $data3);
        //print_r($check);
    }

    if (count($sticky) == 0 && is_array($check)) {
        foreach ($check as $val) {
            $sticky[] = $val['pfkTag'];
        }
        //print_r($sticky);
    }

    foreach ($tags as $tag) {
        
        $output[] = '<label>';
        $output[] = '<input type="checkbox" ' ;
        $output[] = ' name = "tag"';

        // there has to be a better way to make the check boxes sticky as we need to make a array
        //for pmktags as they checked for the checkbox 
        if($tag['pmkTag'] == $Tag) {
            $output[] = ' checked ';
        }
        
        if (in_array($tag['pmkTag'], $sticky)) {
            $output[] = ' checked ';
        }
        
        // output the value for the checkbox
        $output[] = 'value="' . $tag["pmkTag"] . '">' . $tag["pmkTag"];
        $output[] = '</label>';
    }
}
$output[] = '</fieldset>';
//$output[] = '</form>';
print join("\n", $output);  // this prints each line as a separate  line in html
//if ($pmkTrailsId > 0){
//print '<input type="hidden" name="updateID" value="'. $pmkTrailsId . '">';

?>
    
<fieldset class="buttons">
    <input class="button" id="btnSubmit" name="btnSubmit" tabindex="900" type="submit" value="Register" >
</fieldset>

<figure class="topright">
<img src="images/nn.gif" alt="animated image">
</figure>
<?php
print '</form>';
}
include 'footer.php';
?>

